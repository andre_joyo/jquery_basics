document.addEventListener('DOMContentLoaded', function() {
    //Obtener un elemento por su id
    element=document.getElementById("element");
    //Ocultar un determinado elemento
    hide=document.getElementById("hide");
    hide.addEventListener("click", function(){
        element.style.display="none";
    });
    //Mostrar un elemento oculto
    show=document.getElementById("show");
    show.addEventListener("click", function(){
        element.style.display="block";
    })

    var FX = {
        easing: {
            linear: function(progress) {
                return progress;
            },
            quadratic: function(progress) {
                return Math.pow(progress, 2);
            },
            swing: function(progress) {
                return 0.5 - Math.cos(progress * Math.PI) / 2;
            },
            circ: function(progress) {
                return 1 - Math.sin(Math.acos(progress));
            },
            back: function(progress, x) {
                return Math.pow(progress, 2) * ((x + 1) * progress - x);
            },
            bounce: function(progress) {
                for (var a = 0, b = 1, result; 1; a += b, b /= 2) {
                    if (progress >= (7 - 4 * a) / 11) {
                        return -Math.pow((11 - 6 * a - 11 * progress) / 4, 2) + Math.pow(b, 2);
                    }
                }
            },
            elastic: function(progress, x) {
                return Math.pow(2, 10 * (progress - 1)) * Math.cos(20 * Math.PI * x / 3 * progress);
            }
        },
        animate: function(options) {
            var start = new Date;
            var id = setInterval(function() {
                var timePassed = new Date - start;
                var progress = timePassed / options.duration;
                if (progress > 1) {
                    progress = 1;
                }
                options.progress = progress;
                var delta = options.delta(progress);
                options.step(delta);
                if (progress == 1) {
                    clearInterval(id);
                    options.complete();
                }
            }, options.delay || 10);
        },
        fadeOut: function(element, options) {
            var to = 1;
            this.animate({
                duration: options.duration,
                delta: function(progress) {
                    progress = this.progress;
                    return FX.easing.swing(progress);
                },
                complete: options.complete,
                step: function(delta) {
                    element.style.opacity = to - delta;
                }
            });
        },
        fadeIn: function(element, options) {
            var to = 0;
            this.animate({
                duration: options.duration,
                delta: function(progress) {
                    progress = this.progress;
                    return FX.easing.swing(progress);
                },
                complete: options.complete,
                step: function(delta) {
                    element.style.opacity = to + delta;
                }
            });
        }
    };
    //Mostrar un elemento de forma animada con un efecto fundido
    document.getElementById('show_eff').addEventListener('click', function() {
        FX.fadeIn(document.getElementById('element'), {
            duration: 2000,
            complete: function() {
                alert('Complete');
            }
        });
    }, false);
    
    //Ocultar un elemento de forma animada con efecto fundido    
    document.getElementById('hide_eff').addEventListener('click', function() {
        FX.fadeOut(document.getElementById('element'), {
            duration: 2000,
            complete: function() {
                alert('Complete');
            }
        });
    }, false);

    //Iterar una colección de elementos y aplicar un cambio de estilo sobre ellos
    iterator=document.getElementById("iterator");
    iterator.addEventListener("click", function(){
        lista=document.getElementById("lista");
        lc=lista.children;
        for (var i = 0; i < lc.length; i++) {
            console.log(lc[i]);
            red=75*i;
            green=255/i;
            blue=(length-i)*85;
            lc[i].style.color= "rgb("+red+","+green+","+blue+")";
        }
    })
    
    //Obtener el elemento padre de un determinado elemento
    list_item=document.getElementById("list_item");
    console.log(list_item.parentElement);
    //Obtener la colección de hijos de un determinado elemento ( si ese elemento tiene hijos )
    console.log(lista.children);
    //Obtener todos los elementos de una determinada clase ( nos referimos a clase css )
    class_a=document.getElementsByClassName("class_a");
    console.log(class_a);
    //Obtener todos los elementos de una determinada clase y que además están ocultos ( el hecho de que estén ocultos no se obtendrá mediante una clase de css )
    for (var i = 0; i < class_a.length; i++) {
        red=75*i;
        green=255/i;
        blue=(length-i)*85;
        if (class_a[i].style.display== "none" || class_a[i].style.opacity=="0") {
            console.log(class_a[i]);
        }
    }
    //Obtener aquellas opciones de un elemento select que estén seleccionadas ( atributo selected )
    var e = document.getElementById("myselection");
    var selected_option = e.options[e.selectedIndex].value;
    console.log(selected_option)
    //Cambiar el atributo href del primer elemento <a> ( Crea un elemento <a> para poder probar este caso )
    a=document.getElementsByTagName("a")[0];
    a.setAttribute("href", "http://www.google.com/");
    console.log(a);
    //Crear un elemento nuevo <p> con texto dentro del dom de tu página
    p=document.createElement("p");
    text_p=document.createTextNode("Texto");
    p.appendChild(text_p);
    document.body.appendChild(p);
    //Mostrar un alert con el valor del primer <input> de la página ( Crea un elemento <input> para poder probar este caso )
    input=document.getElementsByTagName("input")[0];
    alert(input.value);
    //Eliminar todos los elementos de un determinado selector
    li=document.getElementsByTagName("li");
    len=li.length;
    for (let i = len-1; 0 <= i; i--) {
        li[i].remove();        
        console.log("length: "+li.length)
    }
    //Animar un elemento pasados 2 segundos desde la carga inicial de la página
    setTimeout(function() { 
        document.getElementById("to_animate").animate([
            // keyframes
            { transform: 'translateY(0px)' }, 
            { transform: 'translateY(-300px)' }
          ], { 
            // timing options
            duration: 1000,
            iterations: Infinity
          });
    }, 2000);
})
