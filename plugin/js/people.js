var people=[
    {
        name: "John",
        surname: "Doe",
        career:[
            "UX Designer", 
            "Software Developer"
        ],
        gender: "male"
    },
    {
        name: "Olivia",
        surname: "Brown",
        career:[
            "App Designer",
            "Graphic Designer"
        ],
        gender: "female"
    },
    {
        name: "Amber",
        surname: "Williams",
        career:[
            "Graphic Designer",
            "Software Developer"
        ],
        gender: "female"
    },
    {
        name: "Jack",
        surname: "Brown",
        career:[
            "UX Designer", 
            "Graphic Designer"
        ],
        gender: "male"
    },
    {
        name: "Chelsea",
        surname: "Smith",
        career:[
            "Software Developer",
            "App Designer"
        ],
        gender: "female"
    },
]