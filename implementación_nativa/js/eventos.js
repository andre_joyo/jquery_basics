//Captura que el documento esta disponible
document.addEventListener('DOMContentLoaded', function() {
    var body=document.getElementsByTagName("body")[0];

    tag_h2=document.createElement("h2");
    document.body.appendChild(tag_h2);
    text_h2=document.createTextNode("Página cargada");
    tag_h2.appendChild(text_h2);

    tag_div=document.createElement("div");
    body.appendChild(tag_div);
    text_div=document.createTextNode("Click!")
    tag_div.appendChild(text_div);
    tag_div.setAttribute("id", "to_click");

    tag_div=document.createElement("div");
    body.appendChild(tag_div);
    text_div=document.createTextNode("Hover!")
    tag_div.appendChild(text_div);
    tag_div.setAttribute("id", "to_hover");

    //Captura que se ha hecho click sobre un elemento
    var elemento=document.getElementById("to_click");
    elemento.addEventListener('click', function() {
        alert("Has clickado");
        console.log("Has clickado");
      })

    //Captura que se ha hecho “hover” sobre un elemento
    to_hover=document.getElementById("to_hover");
    to_hover.addEventListener("mouseover", function( event ) {   
        console.log("Hover");
    });

    //Captura cuando el usuario ha pulsado una tecla y obtener el código asociado a dicha tecla
    document.addEventListener('keydown', function(e) {
        console.log(e.keyCode);
    }); 

    //Captura cuando el usuario mueve el mouse y obtener la posición actual del mismo
    document.addEventListener("mousemove", function(){
        console.log("Coordenadas del ratón en la parte visible del navegador: " + event.clientX + ", " + event.clientY);
        console.log("Coordenadas absolutas del ratón en la página actual: " + event.pageX + ", " + event.pageY);
    })
})
