$(document).ready(function(){
    //Ocultar un determinado elemento
    //Obtener un elemento por su id
    $("#hide").click(function(){
        $("#element").hide();
    });
    //Mostrar un elemento oculto
    $("#show").click(function(){
        $("#element").show();
    });
    //Mostrar un elemento de forma animada con un efecto fundido
    $("#show_eff").click(function(){
        $("#element").fadeIn(2000);
    });
    //Ocultar un elemento de forma animada con efecto fundido
    $("#hide_eff").click(function(){
        $("#element").fadeOut(2000);
    });
    //Iterar una colección de elementos y aplicar un cambio de estilo sobre ellos
    counter=0;
    var r=0;
    var g=255;
    var b=127;
    $("#iterator").click(function(){
        $("#lista li").each(function(){
            if (counter==255){
                r=Math.round(Math.random()*255);
                g=Math.round(Math.random()*255);
                b=Math.round(Math.random()*255);
            } else{
                r=r+15;
                g=g-15;
                b=Math.round(Math.random()*255);
                counter=counter+15;    
            }
            $(this).css("background-color", "rgb("+r+","+g+","+b+")");
        });
    });
    //Obtener el elemento padre de un determinado elemento
    console.log($("#list_item").parent()[0]);
    //Obtener la colección de hijos de un determinado elemento ( si ese elemento tiene hijos )
    $("#lista").children().each(function(){
        console.log($(this).text());
    });
    //Obtener todos los elementos de una determinada clase ( nos referimos a clase css )
    $(".class_a").each(function(){
        console.log($(this).text())
    });
    //Obtener todos los elementos de una determinada clase y que además están ocultos ( el hecho de que estén ocultos no se obtendrá mediante una clase de css )
    $(".class_a").each(function(){
        if (!$(this).is(":visible")) {
            console.log("Elemento oculto: "+$(this).text())
        }
    })
    //Obtener aquellas opciones de un elemento select que estén seleccionadas ( atributo selected )
    console.log($("#myselection option:selected").text());
    //Cambiar el atributo href del primer elemento <a> ( Crea un elemento <a> para poder probar este caso )
    $("a:first").attr("href", "http://www.google.com/");
    console.log($("a")[0]);
    //Crear un elemento nuevo <p> con texto dentro del dom de tu página
    $("body").append("<p>Texto</p>");
    //Mostrar un alert con el valor del primer <input> de la página ( Crea un elemento <input> para poder probar este caso )
    alert($("input:first").val());
    //Eliminar todos los elementos de un determinado selector
    $(".to_remove").remove();
    //Animar un elemento pasados 2 segundos desde la carga inicial de la página
    setTimeout(function() { 
        $("#to_animate").animate({marginLeft:"200px"});
    }, 2000);
})
