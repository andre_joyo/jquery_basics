//Captura que el documento esta disponible
$( document ).ready(function() {
    $("body").html("<h2>Página cargada</h2>");
    $("body").append("<div id='to_click'>Click!</div>");
    $("body").append("<div id='to_hover'>Hover!</div>");

    //Captura que se ha hecho click sobre un elemento
    console.log($("div"));
    $("#to_click").click(function(){
        alert("Has clickado");
        console.log("Has clickado");
    });

    //Captura que se ha hecho “hover” sobre un elemento
    $("#to_hover").hover(function(){
        console.log("Hover");
    });

    //Captura cuando el usuario ha pulsado una tecla y obtener el código asociado a dicha tecla
    $( document ).keydown(function() {
        console.log( "Código de tecla pulsada: " + event.which);
    });

    //Captura cuando el usuario mueve el mouse y obtener la posición actual del mismo
    $(document).mousemove(function(event){
        console.log("Coordenadas del ratón en la parte visible del navegador: " + event.clientX + ", " + event.clientY);
        console.log("Coordenadas absolutas del ratón en la página actual: " + event.pageX + ", " + event.pageY);
    });
})
